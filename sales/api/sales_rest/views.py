from django.http import JsonResponse, HttpResponseBadRequest
from django.views import View
from django.core import serializers
from django.forms.models import model_to_dict
from .models import Salesperson, Customer, AutomobileVO, Sale
import json


class SalespersonView(View):
    def get(self, request):
        salespeople = Salesperson.objects.all()
        salespeople_json = [model_to_dict(salesperson)
                            for salesperson in salespeople]
        return JsonResponse(salespeople_json, safe=False)

    def post(self, request):
        try:
            data = json.loads(request.body)
            salesperson = Salesperson.objects.create(**data)
            return JsonResponse(model_to_dict(salesperson), status=201)
        except Exception as e:
            return HttpResponseBadRequest(str(e))

    def delete(self, request, id):
        try:
            salesperson = Salesperson.objects.get(id=id)
            salesperson.delete()
            return JsonResponse({'message': 'Deleted successfully'}, status=200)
        except Salesperson.DoesNotExist:
            return HttpResponseBadRequest('Salesperson does not exist')
        except Exception as e:
            return HttpResponseBadRequest(str(e))


class CustomerView(View):
    def get(self, request):
        customers = Customer.objects.all()
        customers_json = [model_to_dict(customer) for customer in customers]
        return JsonResponse(customers_json, safe=False)

    def post(self, request):
        try:
            data = json.loads(request.body)
            customer = Customer.objects.create(**data)
            return JsonResponse(model_to_dict(customer), status=201)
        except Exception as e:
            return HttpResponseBadRequest(str(e))

    def delete(self, request, id):
        try:
            customer = Customer.objects.get(id=id)
            customer.delete()
            return JsonResponse({'message': 'Deleted successfully'}, status=200)
        except Customer.DoesNotExist:
            return HttpResponseBadRequest('Customer does not exist')
        except Exception as e:
            return HttpResponseBadRequest(str(e))


class SaleView(View):
    def get(self, request):
        sales = Sale.objects.all()
        sales_json = [model_to_dict(sale) for sale in sales]
        return JsonResponse(sales_json, safe=False)

    def post(self, request):
        try:
            data = json.loads(request.body)
            sale = Sale.objects.create(**data)
            return JsonResponse(model_to_dict(sale), status=201)
        except Exception as e:
            return HttpResponseBadRequest(str(e))

    def delete(self, request, id):
        try:
            sale = Sale.objects.get(id=id)
            sale.delete()
            return JsonResponse({'message': 'Deleted successfully'}, status=200)
        except Sale.DoesNotExist:
            return HttpResponseBadRequest('Sale does not exist')
        except Exception as e:
            return HttpResponseBadRequest(str(e))
