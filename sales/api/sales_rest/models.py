from django.db import models

# {
#     "first_name": "Bonnie",
#     "last_name": "Sullivan",
#     "employee_id": 1
# }


class Salesperson(models.Model):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    employee_id = models.IntegerField(unique=True)

# {
#     "first_name": "Bonnie",
#     "last_name": "Sullivan",
#     "address": "123 Some Place",
#     "phone_number": "555-555-5555"
# }


class Customer(models.Model):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    address = models.CharField(max_length=100)
    phone_number = models.CharField(max_length=15)


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, primary_key=True)
    sold = models.BooleanField(default=False)


# {
#   "automobile": "1HGCM82633A123456",
#   "salesperson": 2,
#   "customer": 2,
#   "price": 30000.00
# }


class Sale(models.Model):
    automobile = models.ForeignKey(AutomobileVO, on_delete=models.CASCADE)
    salesperson = models.ForeignKey(Salesperson, on_delete=models.CASCADE)
    customer = models.ForeignKey(Customer, on_delete=models.CASCADE)
    price = models.DecimalField(max_digits=10, decimal_places=2)
