from django.urls import path
from .views import SalespersonView, CustomerView, SaleView

urlpatterns = [
    path('salespeople/', SalespersonView.as_view(), name='salespeople'),
    path('salespeople/<int:id>/', SalespersonView.as_view(), name='salesperson'),
    path('customers/', CustomerView.as_view(), name='customers'),
    path('customers/<int:id>/', CustomerView.as_view(), name='customer'),
    path('sales/', SaleView.as_view(), name='sales'),
    path('sales/<int:id>/', SaleView.as_view(), name='sale'),
]
