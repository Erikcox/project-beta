import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "sales_project.settings")
django.setup()

# Import models from sales_rest, here.
# from sales_rest.models import Something
from sales_rest.models import AutomobileVO


# TESTING
# docker exec -it project-beta-sales-api-1 bash
# docker exec -it project-beta-sales-poller-1 bash
# python test_poller.py

def poll(repeat=True):
    while True:
        print('Sales poller polling for data')
        try:
            response = requests.get(
                "http://project-beta-inventory-api-1:8000/api/automobiles/")
            data = response.json()
            automobiles = data["automobiles"]
            for automobile in automobiles:
                AutomobileVO.objects.update_or_create(
                    sold=automobile["sold"],
                    vin=automobile["vin"]
                )
        except Exception as e:
            print(e, file=sys.stderr)

        if (not repeat):
            break

        time.sleep(60)


if __name__ == "__main__":
    poll()
