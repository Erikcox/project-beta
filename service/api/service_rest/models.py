from django.db import models
from django.urls import reverse


class Technician(models.Model):
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    employee_id = models.PositiveIntegerField(primary_key=True)


# class Status(models.Model):
#     id = models.PositiveSmallIntegerField(primary_key=True)
#     name = models.CharField(max_length=10)

#     def __str__(self):
#         return self.name


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    sold = models.BooleanField(default=False)


class Appointment(models.Model):

    @classmethod
    def create(cls, **kwargs):
        employee_id = kwargs.get("technician")
        technician = Technician.objects.get(employee_id=employee_id)
        kwargs["technician"] = technician
        appointment = cls(**kwargs)
        appointment.save()
        return appointment

    date_time = models.DateTimeField()
    reason = models.CharField(max_length=500)
    vin = models.CharField(max_length=17, unique=True)
    customer = models.CharField(max_length=200)
    status = models.CharField(
        max_length=20,
        choices=[
            ("SUBMITTED", "Submitted"),
            ("CANCELED", "Canceled"),
            ("FINISHED", "Finished")
        ],
        default="SUBMITTED",
        )
    technician = models.ForeignKey(
        Technician,
        related_name="appointments",
        on_delete=models.CASCADE,
    )

    def cancel(self):
        # status = Status.objects.get(name="CANCELED")
        self.status = "CANCELED"
        self.save()

    def finish(self):
        # status = Status.objects.get(name="FINISHED")
        self.status = "FINISHED"
        self.save()
