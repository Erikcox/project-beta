import React, { useState, useEffect } from 'react'

function AppointmentsHistory() {
    const [appointments, setAppointments] = useState([]);
    const [autos, setAutos] = useState([]);
    const [search, setSearch] = useState('');

    async function loadAppointments() {
        const response = await fetch('http://localhost:8080/api/appointments/history/');
        if (response.ok) {
            const data = await response.json();
            setAppointments(data.appointments)
        } else {
            console.log(response);
        }
    }

    async function loadAutos() {
        const response = await fetch('http://localhost:8100/api/automobiles/');
        if (response.ok) {
            const data = await response.json();
            setAutos(data.autos)
        } else {
            console.log(response);
        }
    }

    const handleDelete = async (id) => {
        const response = await fetch(`http://localhost:8080/api/appointments/${id}/`,{ method: "DELETE"});
        if (response.ok) {
            loadAppointments();
        } else {
            console.error('did not delete:', await response.text());
        }
    }

    const handleSearchChange = (event) => {
        setSearch(event.target.value);
    };

    useEffect(() => {
        loadAppointments();
        loadAutos();
    }, [])

    return (
        <div>
            <h1>Service History</h1>
                <div className="form-floating mb-3">
                    <input onChange={handleSearchChange} placeholder="Search by VIN" required type="search" name="search" id="search" style={{ width: '800px' }} value={search} />
                    <button id="I do nothing">Search</button>
                </div>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>VIN</th>
                            <th>is VIP?</th>
                            <th>Customer</th>
                            <th>Date & Time</th>
                            <th>Reason</th>
                            <th>Technician</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        {appointments.filter(appointment => {
                            if (search === '') {
                                return true;
                            } else {
                                return appointment.vin.includes(search);
                            }}).map(appointment => {
                            let isVip = "";
                            let vinList = []
                            for (let auto of autos) {
                                vinList.push(auto.vin);
                            } if (vinList.includes(appointment.vin)) {
                                isVip = "Yes";
                            } else {
                                isVip = "No"
                            } return (
                                <tr key={appointment.id}>
                                    <td>{ appointment.vin }</td>
                                    <td>{ isVip }</td>
                                    <td>{ appointment.customer }</td>
                                    <td>{ appointment.date_time.toLocaleString() }</td>
                                    <td>{ appointment.reason }</td>
                                    <td>{ appointment.technician.first_name} { appointment.technician.last_name }</td>
                                    <td>{ appointment.status }</td>
                                    <td>
                                        <button className= 'btn btn-danger' onClick={() => handleDelete(appointment.id)}>Delete</button>
                                    </td>
                                </tr>
                            );
                        })}
                    </tbody>
                </table>
        </div>
    );
}
export default AppointmentsHistory;
