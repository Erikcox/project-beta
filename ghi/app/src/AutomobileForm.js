import React, { useState, useEffect } from "react";

function AutomobileForm() {
    const [color, setColor] = useState('');
    const [year, setYear] = useState('');
    const [vin, setVin] = useState('');
    const [model, setModel] = useState('');
    const [models, setModels] = useState([]);

    async function loadModels() {
        const response = await fetch('http://localhost:8100/api/models/');
        if (response.ok) {
            const data = await response.json();
            setModels(data.models);
        } else {
            console.log(response);
        }
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const url = 'http://localhost:8100/api/automobiles/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify({
                color: color,
                year: year,
                vin: vin,
                model_id: model,
            }),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            setColor('');
            setYear('');
            setVin('');
            setModel('');
            setModels([]);
        } else {
            console.log(response);
        }
    }

    const handleVinChange = (event) => {
        setVin(event.target.value);
    };

    const handleYearChange = (event) => {
        setYear(event.target.value);
    };

    const handleColorChange = (event) => {
        setColor(event.target.value);
    };

    const handleModelChange = (event) => {
        setModel(event.target.value);
    };

    useEffect(() => {
        loadModels();
    }, [])

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add an Automobile</h1>
                    <form onSubmit={handleSubmit} id="create-automobile-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleVinChange} placeholder="VIN" required type="text" name="vin" id="vin" value={vin} />
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleColorChange}  placeholder="Color" required type="text" name="color" id="color" value={color} />
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleYearChange} placeholder="Year" required type="text" name="year" id="year" value={year} />
                        </div>
                        <div className="mb-3">
                            <select onChange={handleModelChange} required name="model" id="model" className="form-select">
                                <option  value="">Choose a model</option>
                                {models.map(model => {
                                    return (
                                        <option key={model.id} value={model.id}>{model.name}</option>
                                    )
                                })}
                            </select>
                        </div>
                        <button className="btn btn-info">Add</button>
                    </form>
                </div>
            </div>
        </div>
    )

}
export default AutomobileForm;
