import React, { useState, useEffect } from 'react'

function AppointmentsList() {
    const [appointments, setAppointments] = useState([]);
    const [autos, setAutos] = useState([]);

    async function loadAppointments() {
        const response = await fetch('http://localhost:8080/api/appointments/');
        if (response.ok) {
            const data = await response.json();
            setAppointments(data.appointments)
        } else {
            console.log(response);
        }
    }

    async function loadAutos() {
        const response = await fetch('http://localhost:8100/api/automobiles/');
        if (response.ok) {
            const data = await response.json();
            setAutos(data.autos)
        } else {
            console.log(response);
        }
    }

    const handleFinish = async (id) => {
        const response = await fetch(`http://localhost:8080/api/appointments/${id}/finish/`,{ method: "PUT"});
        if (response.ok) {
            loadAppointments();
        } else {
            console.error('did not update to finish:', await response.text());
        }
    }

    const handleCancel = async (id) => {
        const response = await fetch(`http://localhost:8080/api/appointments/${id}/cancel/`,{ method: "PUT"});
        if (response.ok) {
            loadAppointments();
        } else {
            console.error('did not update to cancel:', await response.text());
        }
    }


    useEffect(() => {
        loadAppointments();
        loadAutos();
    }, [])

    return (
        <div>
            <h1>Service Appointments</h1>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>VIN</th>
                            <th>is VIP?</th>
                            <th>Customer</th>
                            <th>Date & Time</th>
                            <th>Reason</th>
                            <th>Technician</th>
                        </tr>
                    </thead>
                    <tbody>
                        {appointments.map(appointment => {
                            let isVip = "";
                            let vinList = []
                            for (let auto of autos) {
                                vinList.push(auto.vin);
                            }
                            if (vinList.includes(appointment.vin)) {
                                isVip = "Yes";
                            } else {
                                isVip = "No"
                            }
                            return (
                                <tr key={appointment.id}>
                                    <td>{ appointment.vin }</td>
                                    <td>{ isVip }</td>
                                    <td>{ appointment.customer }</td>
                                    <td>{ appointment.date_time.toLocaleString() }</td>
                                    <td>{ appointment.technician.first_name} { appointment.technician.last_name }</td>
                                    <td>{ appointment.reason }</td>
                                    <td>
                                        <button className= 'btn btn-info' onClick={() => handleFinish(appointment.id)}>Finish</button>
                                    </td>
                                    <td>
                                        <button className= 'btn btn-danger' onClick={() => handleCancel(appointment.id)}>Cancel</button>
                                    </td>
                                </tr>
                            );
                        })}
                    </tbody>
                </table>
        </div>
    );
}
export default AppointmentsList;
