import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import TechnicianForm from './TechnicianForm';
import AppointmentForm from './AppointmentForm';
import TechniciansList from './TechniciansList';
import AppointmentsList from './AppointmentsList';
import AddSalesperson from './components/AddSalesperson';
import SalespeopleList from './components/SalespeopleList';
import AddCustomer from './components/AddCustomer';
import CustomerList from './components/CustomerList';
import AddSale from './components/AddSale';
import SalesList from './components/SalesList';
import AppointmentsHistory from './AppoinmentsHistory';
import AutomobilesList from './AutomobilesList';
import AutomobileForm from './AutomobileForm';

import AddManufacturer from './components/AddManufacturer';
import ManufacturersList from './components/ManufacturersList';
import AddModel from './components/AddModel';
import ModelsList from './components/ModelsList';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/technicians" element={<TechniciansList />} />
          <Route path="/technicians/new" element={<TechnicianForm />} />
          <Route path="/appointments" element={<AppointmentsList />} />
          <Route path="/appointments/new" element={<AppointmentForm />} />
          <Route path="/appointments/history" element={<AppointmentsHistory />} />
          <Route path="/add-salesperson" element={<AddSalesperson />} />
          <Route path="/salespeople" element={<SalespeopleList />} />
          <Route path="/add-customer" element={<AddCustomer />} />
          <Route path="/customers" element={<CustomerList />} />
          <Route path="/add-sale" element={<AddSale />} />
          <Route path="/sales" element={<SalesList />} />
          <Route path="/automobiles" element={<AutomobilesList />} />
          <Route path="/automobiles/new" element={<AutomobileForm />} />
          <Route path="/add-manufacturer" element={<AddManufacturer />} />
          <Route path="/manufacturers" element={<ManufacturersList />} />
          <Route path="/add-model" element={<AddModel />} />
          <Route path="/models" element={<ModelsList />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
