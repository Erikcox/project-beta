import React, { useState } from 'react';

const AddSalesperson = () => {
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [employeeId, setEmployeeId] = useState('');

    const handleSubmit = (e) => {
        e.preventDefault();

        fetch('http://localhost:8090/api/salespeople/', {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ "first_name": firstName, "last_name": lastName, "employee_id": employeeId })
        })
        .then(response => response.json())
        .then(data => console.log(data))
        .catch((error) => console.error('Error:', error));
    };

    return (
        <form onSubmit={handleSubmit}>
            <label>
                First Name:
                <input type="text" value={firstName} onChange={e => setFirstName(e.target.value)} required />
            </label>
            <label>
                Last Name:
                <input type="text" value={lastName} onChange={e => setLastName(e.target.value)} required />
            </label>
            <label>
                Employee ID:
                <input type="text" value={employeeId} onChange={e => setEmployeeId(e.target.value)} required />
            </label>
            <input type="submit" value="Submit" />
        </form>
    );
};

export default AddSalesperson;
