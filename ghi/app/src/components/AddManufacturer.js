// src/components/AddManufacturer.js
import React, { useState } from 'react';

const AddManufacturer = () => {
  const [manufacturerName, setManufacturerName] = useState('');

  const handleInputChange = (event) => {
    const { value } = event.target;
    setManufacturerName(value);
  };

  const handleSubmit = (event) => {
    event.preventDefault();

    fetch('http://localhost:8100/api/manufacturers/', {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ "name": manufacturerName })
    })
    .then(response => response.json())
    .then(data => console.log(data))
    .catch((error) => console.error('Error:', error));

    // Reset the form field after submission
    setManufacturerName('');
  };

  return (
    <div>
      <h1>Create a Manufacturer</h1>
      <form onSubmit={handleSubmit}>
        <label>
          Manufacturer Name:
          <input
            type="text"
            value={manufacturerName}
            onChange={handleInputChange}
            required
          />
        </label>
        <button type="submit">Create Manufacturer</button>
      </form>
    </div>
  );
};

export default AddManufacturer;
