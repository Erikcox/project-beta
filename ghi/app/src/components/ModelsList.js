// src/components/ModelsList.js
import React, { useState, useEffect } from 'react';

const ModelsList = () => {
  const [vehicleModels, setVehicleModels] = useState([]);

  useEffect(() => {
    // Fetch vehicle models data from the API endpoint
    fetch('http://localhost:8100/api/models/')
      .then((response) => response.json())
      .then((data) => setVehicleModels(data.models))
      .catch((error) => console.error('Error fetching vehicle models:', error));
  }, []);

  return (
    <div>
      <h1>Vehicle Models List</h1>
      <ul>
        {vehicleModels.map((model) => (
          <li key={model.id}>
            <p>Name: {model.name}</p>
            <p>Picture URL: {model.picture_url}</p>
            <p>Manufacturer: {model.manufacturer.name}</p>
          </li>
        ))}
      </ul>
    </div>
  );
};

export default ModelsList;
