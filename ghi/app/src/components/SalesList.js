// src/components/SalesList.js
import React, { useState, useEffect } from 'react';

const SalesList = () => {
  const [sales, setSales] = useState([]);

  useEffect(() => {
    // Fetch sales data from the API endpoint
    fetch('http://localhost:8090/api/sales/')
      .then((response) => response.json())
      .then((data) => setSales(data))
      .catch((error) => console.error('Error fetching sales:', error));
  }, []);

  return (
    <div>
      <h1>Sales List</h1>
      <table>
        <thead>
          <tr>
            <th>Salesperson Name</th>
            <th>Employee ID</th>
            <th>Customer Name</th>
            <th>Automobile VIN</th>
            <th>Price</th>
          </tr>
        </thead>
        <tbody>
          {sales.map((sale) => (
            <tr key={sale.id}>
              <td>{`${sale.salesperson.first_name} ${sale.salesperson.last_name}`}</td>
              <td>{sale.salesperson.employee_id}</td>
              <td>{sale.customer}</td>
              <td>{sale.automobile_vin}</td>
              <td>{sale.price}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default SalesList;
