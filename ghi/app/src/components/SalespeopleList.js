// src/components/SalespeopleList.js
import React, { useState, useEffect } from 'react';

const SalespeopleList = () => {
  const [salespeople, setSalespeople] = useState([]);

  useEffect(() => {
    // Fetch salespeople data from the API endpoint
    fetch('http://localhost:8090/api/salespeople/')
      .then((response) => response.json())
      .then((data) => setSalespeople(data))
      .catch((error) => console.error('Error fetching salespeople:', error));
  }, []);

  return (
    <div>
      <h1>Salespeople List</h1>
      <ul>
        {salespeople.map((salesperson) => (
          <li key={salesperson.employee_id}>
            {`${salesperson.employee_id} - ${salesperson.first_name} ${salesperson.last_name}`}
          </li>
        ))}
      </ul>
    </div>
  );
};

export default SalespeopleList;
