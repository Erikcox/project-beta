// src/components/CustomerList.js
import React, { useState, useEffect } from 'react';

const CustomerList = () => {
  const [customers, setCustomers] = useState([]);

  useEffect(() => {
    // Fetch customer data from the API endpoint
    fetch('http://localhost:8090/api/customers/')
      .then((response) => response.json())
      .then((data) => setCustomers(data))
      .catch((error) => console.error('Error fetching customers:', error));
  }, []);

  return (
    <div>
      <h1>Customers List</h1>
      <ul>
        {customers.map((customer) => (
          <li key={customer.id}>
            {`${customer.first_name} ${customer.last_name} - ${customer.phone_number} - ${customer.address}`}
          </li>
        ))}
      </ul>
    </div>
  );
};

export default CustomerList;
