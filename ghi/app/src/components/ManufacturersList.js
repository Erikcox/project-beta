// src/components/ManufacturersList.js
import React, { useState, useEffect } from 'react';

const ManufacturersList = () => {
  const [manufacturers, setManufacturers] = useState([]);

  useEffect(() => {
    // Fetch manufacturers data from the API endpoint
    fetch('http://localhost:8100/api/manufacturers/')
      .then((response) => response.json())
      .then((data) => setManufacturers(data.manufacturers))
      .catch((error) => console.error('Error fetching manufacturers:', error));
  }, []);

  return (
    <div>
      <h1>Manufacturers List</h1>
      <ul>
        {manufacturers.map((manufacturer) => (
          <li key={manufacturer.id}>
            <p>ID: {manufacturer.id}</p>
            <p>Name: {manufacturer.name}</p>
            <p>Href: {manufacturer.href}</p>
          </li>
        ))}
      </ul>
    </div>
  );
};

export default ManufacturersList;
