// src/components/AddModel.js
import React, { useState } from 'react';

const AddModel = () => {
  const [modelName, setModelName] = useState('');
  const [pictureUrl, setPictureUrl] = useState('');
  const [manufacturerId, setManufacturerId] = useState('');

  const handleInputChange = (event) => {
    const { name, value } = event.target;
    if (name === 'modelName') {
      setModelName(value);
    } else if (name === 'pictureUrl') {
      setPictureUrl(value);
    } else if (name === 'manufacturerId') {
      setManufacturerId(value);
    }
  };

  const handleSubmit = (event) => {
    event.preventDefault();

    fetch('http://localhost:8100/api/models/', {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ "name": modelName, "picture_url": pictureUrl, "manufacturer_id": manufacturerId })
    })
    .then(response => response.json())
    .then(data => console.log(data))
    .catch((error) => console.error('Error:', error));

    // Reset the form fields after submission
    setModelName('');
    setPictureUrl('');
    setManufacturerId('');
  };

  return (
    <div>
      <h1>Create a Vehicle Model</h1>
      <form onSubmit={handleSubmit}>
        <label>
          Model Name:
          <input
            type="text"
            name="modelName"
            value={modelName}
            onChange={handleInputChange}
            required
          />
        </label>
        <label>
          Picture URL:
          <input
            type="text"
            name="pictureUrl"
            value={pictureUrl}
            onChange={handleInputChange}
            required
          />
        </label>
        <label>
          Manufacturer ID:
          <input
            type="text"
            name="manufacturerId"
            value={manufacturerId}
            onChange={handleInputChange}
            required
          />
        </label>
        <button type="submit">Create Model</button>
      </form>
    </div>
  );
};

export default AddModel;
