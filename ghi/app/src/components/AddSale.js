// src/components/AddSale.js
import React, { useState } from 'react';

const AddSale = () => {
  const [formData, setFormData] = useState({
    automobile: '',
    salesperson: '',
    customer: '',
    price: '',
  });

  const handleInputChange = (event) => {
    const { name, value } = event.target;
    setFormData({ ...formData, [name]: value });
  };

  const handleSubmit = (event) => {
    event.preventDefault();

    fetch('http://localhost:8090/api/sales/', {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ "automobile": formData.automobile, "salesperson": formData.salesperson, "customer": formData.customer, "price": formData.price })
        })
        .then(response => response.json())
        .then(data => console.log(data))
        .catch((error) => console.error('Error:', error));

    // Reset the form fields after submission
    setFormData({
      automobile: '',
      salesperson: '',
      customer: '',
      price: '',
    });
  };

  return (
    <div>
      <h1>Record a New Sale</h1>
      <form onSubmit={handleSubmit}>
        <label>
          Automobile VIN:
          <select
            name="automobile"
            value={formData.automobile}
            onChange={handleInputChange}
            required
          >
            <option value="">Select an Automobile</option>
            {/* Add options dynamically based on the available automobiles */}
            {/* Example: */}
            <option value="vin123">Automobile 1 (VIN: vin123)</option>
            <option value="vin456">Automobile 2 (VIN: vin456)</option>
            {/* Add more options as needed */}
          </select>
        </label>
        <label>
          Salesperson:
          <select
            name="salesperson"
            value={formData.salesperson}
            onChange={handleInputChange}
            required
          >
            <option value="">Select a Salesperson</option>
            {/* Add options dynamically based on the available salespeople */}
            {/* Example: */}
            <option value="sp1">Salesperson 1</option>
            <option value="sp2">Salesperson 2</option>
            {/* Add more options as needed */}
          </select>
        </label>
        <label>
          Customer:
          <select
            name="customer"
            value={formData.customer}
            onChange={handleInputChange}
            required
          >
            <option value="">Select a Customer</option>
            {/* Add options dynamically based on the available customers */}
            {/* Example: */}
            <option value="cust1">Customer 1</option>
            <option value="cust2">Customer 2</option>
            {/* Add more options as needed */}
          </select>
        </label>
        <label>
          Price:
          <input
            type="text"
            name="price"
            value={formData.price}
            onChange={handleInputChange}
            required
          />
        </label>
        <button type="submit">Record Sale</button>
      </form>
    </div>
  );
};

export default AddSale;
