import React, { useState } from 'react';

function TechnicianForm() {
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [employeeId, setEmployeeId] = useState('');

    const handleSubmit = async (event) => {
        event.preventDefault();
        const url = "http://localhost:8080/api/technicians/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify({
                first_name: firstName,
                last_name: lastName,
                employee_id: employeeId,
            }),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            setFirstName('');
            setLastName('');
            setEmployeeId('');
        }
    }

    const handleFirstNameChange = (event) => {
        setFirstName(event.target.value);
    };

    const handleLastNameChange = (event) => {
        setLastName(event.target.value);
    };

    const handleEmployeeIdChange = (event) => {
        setEmployeeId(event.target.value);
    };



    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add a Technician</h1>
                    <form className='' onSubmit={handleSubmit} id="create-technicians-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleFirstNameChange} placeholder="First Name" required type="text" name="first_name" id="first_name" value={firstName} />
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleLastNameChange}  placeholder="Last Name" required type="text" name="last_name" id="last_name" value={lastName} />
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleEmployeeIdChange} placeholder="Employee Id" required type="number" name="employee_id" id="employee_id" value={employeeId} />
                        </div>
                        <button className="btn btn-primary">Add</button>
                    </form>
                </div>
            </div>
        </div>
    )
}
export default TechnicianForm;
