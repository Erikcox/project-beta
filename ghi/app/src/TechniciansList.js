import React, { useState, useEffect } from 'react';


function TechniciansList() {
    const [technicians, setTechnicians] = useState([]);

    async function loadTechnicians() {
        const response = await fetch('http://localhost:8080/api/technicians/');
        if (response.ok) {
            const data = await response.json();
            setTechnicians(data.technicians)
        } else {
            console.log(response);
        }
    }

    const handleDelete = async (employeeId) => {
        const response = await fetch(`http://localhost:8080/api/technicians/${employeeId}/`,{ method: "DELETE"});
        if (response.ok) {
            loadTechnicians();
        } else {
            console.error('did not delete:', await response.text());
        }
    }

    useEffect(() => {
        loadTechnicians();
    }, [])

    return (
        <div>
            <h1>Technicians</h1>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>Employee Id</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                        </tr>
                    </thead>
                    <tbody>
                        {technicians.map(technician => {
                            return (
                                <tr key={technician.employee_id}>
                                    <td>{ technician.employee_id }</td>
                                    <td>{ technician.first_name }</td>
                                    <td>{ technician.last_name }</td>
                                    <td>
                                        <button className= 'btn btn-info' onClick={() => handleDelete(technician.employee_id)}>Delete</button>
                                    </td>
                                </tr>
                            );
                        })}
                    </tbody>
                </table>
        </div>
    );
}
export default TechniciansList;
