import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li>
              <NavLink className="nav-link" to="/technicians/new">Add a Technician</NavLink>
            </li>
            <li>
              <NavLink className="nav-link" to="/appointments/new">Create an Appointment</NavLink>
            </li>
            <li>
              <NavLink className="nav-link" to="/technicians">List Technicians</NavLink>
            </li>
            <li>
              <NavLink className="nav-link" to="/appointments">List Current Appointments</NavLink>
            </li>
            <li>
              <NavLink className="nav-link" to="/appointments/history">List Appointments History</NavLink>
            </li>
            <li>
              <NavLink className="nav-link" to="/automobiles">List Automobiles</NavLink>
            </li>
            <li>
              <NavLink className="nav-link" to="/automobiles/new">Add an Automobile</NavLink>
            </li>
             <li>
                <NavLink to="/add-salesperson">Add Salesperson</NavLink>
              </li>
              <li>
                <NavLink to="/salespeople">List Salespeople</NavLink>
              </li>
              <li>
                <NavLink to="/add-customer">Add Customer</NavLink>
              </li>
              <li>
                <NavLink to="/customers">List Customers</NavLink>
              </li>
              <li>
                <NavLink to="/add-sale">Add Sale</NavLink>
              </li>
              <li>
                <NavLink to="/sales">List Sale</NavLink>
              </li>
              <li>
                <NavLink to="/manufacturers">List Manufacturers</NavLink>
              </li>
              <li>
                <NavLink to="/add-manufacturer">Add Manufacturer</NavLink>
              </li>
              <li>
                <NavLink to="/models">List Vehicle Models</NavLink>
              </li>
              <li>
                <NavLink to="/add-model">Add Vehicle Model</NavLink>
              </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
