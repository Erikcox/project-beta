import React, { useState,useEffect } from 'react';

function AppointmentForm() {
    const [vin, setVin] = useState('');
    const [customer, setCustomer] = useState('');
    const [dateTime, setDateTime] = useState('');
    const [reason, setReason] = useState('');
    const [technicians, setTechnicians] = useState([])
    const [technician, setTechnician] = useState('')

    async function loadTechnicians() {
        const response = await fetch('http://localhost:8080/api/technicians/');
        if (response.ok) {
            const data = await response.json();
            setTechnicians(data.technicians);
        } else {
            console.log(response);
        }
    }
    useEffect(() => {
        loadTechnicians();
    }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();
        const url = 'http://localhost:8080/api/appointments/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify({
                date_time: dateTime,
                reason: reason,
                vin: vin,
                customer: customer,
                technician: technician,
            }),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            setVin('');
            setCustomer('');
            setDateTime('');
            setReason('');
            setTechnician('');
            setTechnicians([]);
        } else {
            console.log(response);
        }
    }

    const handleVinChange = (event) => {
        setVin(event.target.value);
    };

    const handleCustomerChange = (event) => {
        setCustomer(event.target.value);
    };

    const handleReasonChange = (event) => {
        setReason(event.target.value);
    };

    const handleDateTimeChange = (event) => {
        setDateTime(event.target.value);
    };

    const handleTechnicianChange = (event) => {
        setTechnician(event.target.value);
    };

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a Service Appointment</h1>
                    <form onSubmit={handleSubmit} id="create-appointment-form">
                    <label htmlFor="vin">Automobile VIN</label>
                        <div className="form-floating mb-3">
                            <input onChange={handleVinChange} placeholder="VIN" required type="text" name="vin" id="vin" value={vin} />
                        </div>
                        <label htmlFor="customer">Customer</label>
                        <div className="form-floating mb-3">
                            <input onChange={handleCustomerChange}  placeholder="First Last" required type="text" name="customer" id="customer" value={customer} />
                        </div>
                        <label htmlFor="datetime">Date & Time</label>
                        <div className="form-floating mb-3">
                            <input onChange={handleDateTimeChange} placeholder="Date & Time" required type="datetime-local" name="datetime" id="datetime" value={dateTime} />
                        </div>
                        <label htmlFor="reason">Reason</label>
                        <div className="form-floating mb-3">
                            <textarea onChange={handleReasonChange} placeholder='your text here' required type="text" name="reason" id="reason" value={reason} />
                        </div>
                        <label htmlFor="technician">Choose a Technician</label>
                        <div className="mb-3">
                            <select onChange={handleTechnicianChange} required name="technician" id="technician" className="form-select">
                                <option value=""></option>
                                {technicians.map(technician => {
                                    return (
                                        <option key={technician.employee_id} value={technician.employee_id}>{technician.first_name} {technician.last_name}</option>
                                    )
                                })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    )
}
export default AppointmentForm;
