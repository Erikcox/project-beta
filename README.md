# CarCar

Team:

* Ben Spak - Sales Microservice
* Erik Cox - Service Microservice

## Microservices Diagram

https://excalidraw.com/#room=0698e2a66d1036c488f1,YJwf-C1CsM7rf-QZHHlE3g

## Repo

https://gitlab.com/Erikcox/project-beta

## Getting Started
You must have docker desktop downloaded to run the commands below.

1. Clone the repo: git clone https://gitlab.com/Erikcox/project-beta
2. run `docker volume create beta-data`
3. Run `docker-compose build`
4. Run `docker-compose up`

- View the project at http://localhost:3000/
- it sometimes takes a couple minutes to start working, if you see "VM668:5 crbug/1173575, non-JS module files deprecated" this error, wait a few minutes and it should work.
## Service Microservice

Technician: This model is for a person who works in services, it has first_name, last_name, and employee_id. I made employee_id a positive integer so that it would be easier to reference. I also set the employee_id to the primary key so that unique would be true and it would not create an extra id automatically. i want to just reference them by the employee_id.

AutomobileVO: this model is for the automobiles in the database to turn them into value objects with just the vin and whether or not they have been sold.

Appointment: this model is for handling service appointments. it has properties date time, vin, reason, customer, status, and technician as a foreign key. it has a class method for creation where it grabs the technician id we asked for on submission and ties it to the created appointment. I also set the status to have three options for submitted, finished, and canceled, with the default set to submitted. I added method in the class to be able to change the status to either canceled or finished.

In service/api/service_rest/views.py, I created views for my models to be able to Delete, Create, and get a list of the objects. For appointments I also made Update views for the cancel and finish features. I then Pthed them in the urls.py in the same directory, also including these urls in the service_project urls.py.

On the Front-end I created Components for each of the list views and Form components for the Create ones. The list components have the buttons for Deletion and Updating(just apoinments have Updating). The navigation links are handled in Nav.js, While the Routing is in our App.js.



## Design

We created Restful API endpoints in our Django back-end and our React front-end calls to those.

## Sales Microservice

### Sales Models

Salesperson: This model represents a person who is responsible for selling the automobiles. Each salesperson has a first_name, last_name, and employee_id.

Customer: This model represents a customer who is looking to buy an automobile. A Customer has fields for first_name, last_name, address, and phone_number.

AutomobileVO: This model is a representation of an automobile in your inventory. Each AutomobileVO has a vin (Vehicle Identification Number) field, which is unique for each car, and a sold field to indicate whether the car has been sold or not.

Sale: This model represents a transaction where an automobile is sold by a salesperson to a customer. It includes fields for the automobile (a foreign key to the AutomobileVO), salesperson (a foreign key to the Salesperson), customer (a foreign key to the Customer), and the price of the sale.

### Integration

The on_delete=models.CASCADE argument on the foreign keys means that when the referenced object is deleted, also delete the objects that have references to them (i.e., when a Salesperson, Customer, or AutomobileVO is deleted, all related Sale objects will be deleted as well).

Integration with an inventory microservice would occur at the AutomobileVO and Sale models:

AutomobileVO: This model can be used to sync data from the inventory microservice. When a new car is added to the inventory, a corresponding AutomobileVO object can be created in this service with the sold field set to False. When a car is removed from the inventory, the corresponding AutomobileVO object can be deleted in this service.

Sale: Whenever a new Sale object is created (i.e., a car is sold), you would update the corresponding AutomobileVO object's sold field to True, and also send a request to the inventory microservice to mark the car as sold there as well. This ensures that a car cannot be sold twice, as any attempts to create a Sale object with a AutomobileVO that has sold=True should be rejected.

### Sales API Endpoints

| Action                       | Method | URL                                         |
|------------------------------|--------|---------------------------------------------|
| List salespeople             | GET    | http://localhost:8090/api/salespeople/      |
| Create a salesperson         | POST   | http://localhost:8090/api/salespeople/      |
| Delete a specific salesperson| DELETE | http://localhost:8090/api/salespeople/:id   |
| List customers               | GET    | http://localhost:8090/api/customers/        |
| Create a customer            | POST   | http://localhost:8090/api/customers/        |
| Delete a specific customer   | DELETE | http://localhost:8090/api/customers/:id     |
| List sales                   | GET    | http://localhost:8090/api/sales/            |
| Create a sale                | POST   | http://localhost:8090/api/sales/            |
| Delete a sale                | DELETE | http://localhost:8090/api/sales/:id         |

### Sales Front-end Routes

| Action                       | Method | URL                                         |
|------------------------------|--------|---------------------------------------------|
| List salespeople             | GET    | http://localhost:3000/salespeople           |
| Create a salesperson         | POST   | http://localhost:3000/add-salesperson       |
| List customers               | GET    | http://localhost:3000/customers             |
| Create a customer            | POST   | http://localhost:3000/add-customer          |
| List sales                   | GET    | http://localhost:3000/sales                 |
| Create a sale                | POST   | http://localhost:3000/add-sale              |

### Services API Endpoints

| Action                       | Method | URL                                               |
|------------------------------|--------|---------------------------------------------------|
| List technicians             | GET    | http://localhost:8080/api/technicians/            |
| Create a technician          | POST   | http://localhost:8080/api/technicians/            |
| Delete a specific technician | DELETE | http://localhost:8080/api/technicians/:id/        |
| List submitted appointments  | GET    | http://localhost:8080/api/appointments/           |
| List appointment history     | GET    | http://localhost:8080/api/appointments/history/   |
| Delete an appointment        | DELETE | http://localhost:8080/api/appointments/:id/       |
| Create an appointment        | POST   | http://localhost:8080/api/appointments/           |
| Set appointment as canceled  | PUT    | http://localhost:8080/api/appointments/:id/cancel/|
| Set appointment as finished  | PUT    | http://localhost:8080/api/appointments/:id/finish/|

** `Must create a technician before creating any appointments so that you can choose a technician.` **

`Example Json for creating a technician`
{
	"first_name": "John",
	"last_name": "Doe",
	"employee_id": 2479
}
`Output will be the same^^`

`Example Json for creating an appointment`
{
	"date_time": "2023-10-28 16:30:00",
	"reason": "oil change",
	"vin": "ABC33333333333333",
	"customer": "James Doe",
	"technician": 2479
}

`output`
{
	"id": 4,
	"date_time": "2023-10-28 16:30:00",
	"reason": "oil change",
	"vin": "ABC33333333333333",
	"customer": "James Doe",
	"technician": {
		"first_name": "John",
		"last_name": "Doe",
		"employee_id": 2479
	},
	"status": "SUBMITTED"
}

## Inventory

### Inventory API Endpoints

| Action                       | Method | URL                                         |
|------------------------------|--------|---------------------------------------------|
| List manufacturers           | GET    | http://localhost:8100/api/manufacturers/    |
| Create a manufacturer        | POST   | http://localhost:8100/api/manufacturers/    |
| List vehicle models          | GET    | http://localhost:8100/api/models/           |
| Create a vehicle model       | POST   | http://localhost:8100/api/models/           |
| List automobiles             | GET    | http://localhost:8100/api/automobiles/      |
| Create an automobile         | POST   | http://localhost:8100/api/automobiles/      |

### Inventory Front-end Routes

| Action                       | Method | URL                                         |
|------------------------------|--------|---------------------------------------------|
| List manufacturers           | GET    | http://localhost:3000/manufacturers         |
| Create a manufacturer        | POST   | http://localhost:3000/add-manufacturers     |
| List vehicle models          | GET    | http://localhost:3000/models                |
| Create a vehicle model       | POST   | http://localhost:3000/add-model             |
| List automobiles             | GET    | http://localhost:3000/automobiles           |
| Create an automobile         | POST   | http://localhost:3000/automobiles/new       |

** `Must create a manufacturer before creating a vehicle model, and create a vehicle model before an automobile` **

`Example Json for creating a manufacturer`
{
  "name": "Toyota"
}

`Output`
{
	"href": "/api/manufacturers/5/",
	"id": 5,
	"name": "Toyota"
}

`Example Json for creating a vehicle model`
{
  "name": "Sebring",
  "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
  "manufacturer_id": 1
}

`Output`
{
	"href": "/api/models/1/",
	"id": 1,
	"name": "Sebring",
	"picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
	"manufacturer": {
		"href": "/api/manufacturers/1/",
		"id": 1,
		"name": "Chrysler"
	}
}

`Example Json for creating an automobile`
{
  "color": "red",
  "year": 2012,
  "vin": "1C3CC5FB2AN120174",
  "model_id": 1
}

`Output`
{
	"href": "/api/automobiles/1C3CC5FB2AN120174/",
	"id": 1,
	"color": "red",
	"year": 2012,
	"vin": "1C3CC5FB2AN120174",
	"model": {
		"href": "/api/models/1/",
		"id": 1,
		"name": "Sebring",
		"picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
		"manufacturer": {
			"href": "/api/manufacturers/1/",
			"id": 1,
			"name": "Chrysler"
		}
	},
	"sold": false
}
